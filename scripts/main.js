"use strict"

const storyPoints = [2, 4, 6];

const backLog = [2, 3, 6, 8, 12, 125];

// розрахунок кількості робочих днів

const deadline = new Date(2023, 2, 13);
const startDay = new Date();
let daysBetween = (deadline - startDay)/(1000*60*60*24) + 1;
let weeksFull = daysBetween/7;
let workDays;

if (weeksFull % 1 === 0 ) {
     workDays = weeksFull * 5;
} if (deadline.getDay() > startDay.getDay() && deadline.getDay() !== 6) {
    workDays = (weeksFull - weeksFull % 1)*5 + (deadline.getDay() - startDay.getDay())
} if (deadline.getDay() < startDay.getDay() && deadline.getDay !== 0) {
    workDays = (weeksFull - weeksFull % 1)*5 + (deadline.getDay() + 5 - startDay.getDay())
}


const storyPointsAll = storyPoints.reduce((sum, el) => sum + el, 0);

const backLogFull = backLog.reduce((sum, elem) => sum + elem, 0);

const showProductivity = (job, time, term) => job / time <= term ? 
alert(`Завдання будуть успішно виконані за ${Math.ceil(term - job / time)} днів до настання дедлайну!`) : 
alert(`Команді розробників доведеться витратити додатково ${(job / time - term)*8} годин після дедлайну, щоб виконати всі завдання в беклозі`);

showProductivity(backLogFull, storyPointsAll, workDays)


// console.log(workDays) 
// console.log(backLogFull/storyPointsAll)
// console.log(storyPointsAll)
// console.log(backLogFull)
